import configparser
import collections
import time
import json
import sys
from TM1py.Services import TM1Service
from TM1py.Objects.ElementAttribute import ElementAttribute
from TM1py.Objects.Element import Element
from TM1py.Utils.Utils import CaseAndSpaceInsensitiveDict, CaseAndSpaceInsensitiveTuplesDict
from TM1py.Objects.TM1Object import TM1Object
from TM1py.Objects.Hierarchy import Hierarchy
import numpy as np
import cplex
from cplex import Cplex
from cplex.exceptions import CplexError
from docplex.cp.model import CpoModel
import random
from random import randint
import math

# variavel de decisao do estoque novo modelo discutido com o marcelo rodrigues

with TM1Service(address='localhost', port=81240, namespace='Harmony LDAP', user='administrator', password='IBMDem0s', ssl=False) as tm1:
    cubo_demanda = 'free_demand'
    cubo_eficiencia_energetica = 'eficiencia_energetica_FIAT'
    cubo_capacidade_producao = 'capacidade'
    cubo_consultivo = 'consultivo_FIAT'
    cubo_veiculo_margin = 'margem_FIAT'
    cubo_resultado = 'resultado_FIAT_ORIGINAL'
    cubo_pode_produzir = 'pode_produzir_FIAT'
    cubo_estoque = 'giro_consultivo'
    cubo_tipo_combustivel = 'tipo_combustivel'
    cubo_wholesales_resultado = 'wholesales_resultado'
    view_name = 'dados'
    meses_nome =  ['JAN','FEB','MAR','ABR','JUN','JUL','AGO','SET','OUT','NOV','DEZ']

    #demanda
    carros =[]
    meses = []
    mes_atual = 0 #abril
    data = tm1.cubes.cells.execute_view(cubo_demanda, view_name, private=False)
    totalizador = "Total"
    for i in data:
        dim0 = i[0].split('].[')[2].replace("]","")
        dim1 = i[1].split('].[')[2].replace("]","")
        if dim0 not in carros and dim0 != totalizador:
            carros.append(dim0)
        if dim1 not in meses and dim1 != totalizador:
            meses.append(dim1)
    # cellset = {}
    # for i in carros:
    #     for j in meses:
    #         cellset[(i,'Limite Inferior')] = 1
    #         cellset[(i,'Limite Superior')] = random.uniform(0,1)
    #         cellset[(i,'Estoque Final')] = randint(300,700)
    #         cellset[(i,'Estoque Inicial')] = randint(300,400)
    # tm1.cubes.cells.write_values(cubo_estoque, cellset)
    # sys.exit()

    d = [[float('inf') for _ in meses] for _ in carros]
    original = [[float('inf') for _ in meses] for _ in carros]
    ls = [[float('inf') for _ in meses] for _ in carros]
    wholesales = [[float('inf') for _ in meses] for _ in carros]
    wholesales_s = [[float('inf') for _ in meses] for _ in carros]
    wholesales_i = [[float('inf') for _ in meses] for _ in carros]

    for i in data:
        if_totalizador = True
        dim0 = i[0].split('].[')[2].replace("]","")
        dim1 = i[1].split('].[')[2].replace("]","")
        dim2 = i[2].split('].[')[2].replace("]","")
        dim3 = i[3].split('].[')[2].replace("]","")
        if dim0 == totalizador:
            if_totalizador = False
            continue
        if dim1 == totalizador:
            if_totalizador = False
            continue
        if if_totalizador:
            iCarro = carros.index(dim0)
            iMes = meses.index(dim1)
            values = data.get(i)['Value']
            if values is  None:
                values = 0
            if dim2 == 'delivery':
                if(dim3 == 'value'):
                    original[iCarro][iMes] = values
                if(dim3 == 'limite inferior'):
                    d[iCarro][iMes] = original[iCarro][iMes]*((1-values))
                if(dim3 == 'limite superior'):
                    ls[iCarro][iMes] = original[iCarro][iMes]*((1+values))
            if dim2 == 'wholesalees':
                if(dim3 == 'value'):
                    wholesales[iCarro][iMes] = values
                if(dim3 == 'limite inferior'):
                    wholesales_i[iCarro][iMes] = wholesales[iCarro][iMes]*((1-values))
                if(dim3 == 'limite superior'):
                    wholesales_s[iCarro][iMes] = wholesales[iCarro][iMes]*((1+values))
    # print()
    # estoque  
    
    # print(estoque_final_s,estoque_final_i)
    # sys.exit()
    #eficiencia
    B = 12
    e = [float('inf') for _ in carros]
    data = tm1.cubes.cells.execute_view(cubo_eficiencia_energetica, view_name, private=False)
    for i in data:
        if_totalizador = True
        dim0 = i[0].split('].[')[2].replace("]","")
        if dim0 == totalizador:
            if_totalizador = False
            continue
        if if_totalizador:
            iCarro = carros.index(dim0)
            values = data.get(i)['Value']
            if values is  None:
                values = 0
            e[iCarro] = values

    print('eficiencia')
    #margin de contribuição
    c = [[float('inf') for _ in meses] for _ in carros]

    data = tm1.cubes.cells.execute_view(cubo_veiculo_margin, view_name, private=False)
    for i in data:
        if_totalizador = True
        dim0 = i[0].split('].[')[2].replace("]","")
        dim1 = i[1].split('].[')[2].replace("]","")
        # print(dim0,dim1)
        # print(meses_nome[mes_atual])
        # if dim1 != meses[mes_atual]:
        #     if_totalizador = False
        #     continue
        if dim0 == totalizador:
            if_totalizador = False
            continue
        if dim1 == totalizador:
            if_totalizador = False
            continue
        if if_totalizador:
            iCarro = carros.index(dim0)
            iMes = meses.index(dim1)
            values = data.get(i)['Value']
            if values is  None:
                values = 0
            c[iCarro][iMes] = values
    print('contribuicao')

    # capacidade de produção
    linhas = []
    plantas = []
    data = tm1.cubes.cells.execute_view(cubo_capacidade_producao, view_name, private=False)
    totalizador = "Total"
    for i in data:
        dim0 = i[0].split('].[')[2].replace("]","")
        dim1 = i[1].split('].[')[2].replace("]","")
        if dim0 not in linhas and dim0 != totalizador:
            linhas.append(dim0)
        if dim1 not in plantas and dim1 != totalizador:
            plantas.append(dim1)

    k = [[[float('inf') for _ in meses] for _ in linhas] for _ in plantas]
    for i in data:
        if_totalizador = True
        dim0 = i[0].split('].[')[2].replace("]","")
        dim1 = i[1].split('].[')[2].replace("]","")
        dim2 = i[2].split('].[')[2].replace("]","")
        if dim0 == totalizador:
            if_totalizador = False
            continue
        if dim1 == totalizador:
            if_totalizador = False
            continue
        if dim2 == totalizador:
            if_totalizador = False
            continue
        if if_totalizador:
            iLinha = linhas.index(dim0)
            iPlanta = plantas.index(dim1)
            iMes = meses.index(dim2)
            values = data.get(i)['Value']
            if values is  None:
                values = 0
            k[iPlanta][iLinha][iMes] = values

    print('capacidade')

    #verifica se linha planta pode produzir mvs 1 = true 0 = false
    pode_produzir = [[[int(0) for _ in carros] for _ in linhas] for _ in plantas]
    data = tm1.cubes.cells.execute_view(cubo_pode_produzir, view_name, private=False)
    num_restricao_produzir = 0
    for i in data:
        if_totalizador = True
        dim0 = i[0].split('].[')[2].replace("]","")
        dim1 = i[1].split('].[')[2].replace("]","")
        dim2 = i[2].split('].[')[2].replace("]","")
        if dim0 == totalizador:
            if_totalizador = False
            continue
        if dim1 == totalizador:
            if_totalizador = False
            continue
        if dim2 == totalizador:
            if_totalizador = False
            continue
        if if_totalizador:
            iCarro = carros.index(dim2)
            iPlanta = plantas.index(dim0)
            iLinha = linhas.index(dim1)
            values = data.get(i)['Value']
            if values is  None:
                values = 0
                num_restricao_produzir+=1
            pode_produzir[iPlanta][iLinha][iCarro] = values

    # estoque
    # cellset = {}
    # for v in carros:
    #     for i in plantas:
    #         cellset[(v,i,'Giro Final','PO 08 2018')] = randint(50,200)
    # tm1.cubes.cells.write_values(cubo_estoque, cellset)
    # sys.exit()
    estoque_inicial = [[float('inf')  for _ in carros] for _ in plantas]
    estoque_final = [[float('inf') for _ in carros] for _ in plantas]
    estoque_final_s = [[float('inf')  for _ in carros] for _ in plantas]
    estoque_final_i = [[float('inf')  for _ in carros] for _ in plantas]
    data = tm1.cubes.cells.execute_view(cubo_estoque, view_name, private=False)
    for i in data:
        if_totalizador = True
        dim0 = i[0].split('].[')[2].replace("]","")
        dim1 = i[2].split('].[')[2].replace("]","")
        dim2 = i[1].split('].[')[2].replace("]","")
        if dim0 == totalizador:
            if_totalizador = False
            continue
        if dim1 == totalizador:
            if_totalizador = False
            continue
        if dim2 == totalizador:
            if_totalizador = False
            continue
        if if_totalizador:
            iCarro = carros.index(dim0)
            iPlanta = plantas.index(dim2)
            values = data.get(i)['Value']
            if(dim1 == 'Giro Inicial'):
                if values is  None:
                    values = 0
                estoque_inicial[iPlanta][iCarro] = values
            if(dim1 == 'Giro Final'):
                if values is  None:
                    values = 0
                estoque_final[iPlanta][iCarro] = values
            if(dim1 == 'Limite Inferior'):
                if values is  None:
                    values = 0
                estoque_final_i[iPlanta][iCarro] = estoque_final[iPlanta][iCarro]*((1-values))
            if(dim1 == 'Limite Superior'):
                if values is  None:
                    values = 0
                estoque_final_s[iPlanta][iCarro] = estoque_final[iPlanta][iCarro]*((1+values))
    # print(estoque_final_s)
    # sys.exit()

    #consultivo
    x = [[[[int(0) for _ in meses] for _ in carros] for _ in linhas] for _ in plantas]
    data = tm1.cubes.cells.execute_view(cubo_consultivo, view_name, private=False)
    for i in data:
        if_totalizador = True
        dim0 = i[0].split('].[')[2].replace("]","")
        dim1 = i[1].split('].[')[2].replace("]","")
        dim2 = i[2].split('].[')[2].replace("]","")
        dim3 = i[3].split('].[')[2].replace("]","")
        if dim0 == totalizador:
            if_totalizador = False
            continue
        if dim1 == totalizador:
            if_totalizador = False
            continue
        if dim2 == totalizador:
            if_totalizador = False
            continue
        if dim3 == totalizador:
            if_totalizador = False
            continue
        if if_totalizador:
            iMes = meses.index(dim2)
            iCarro = carros.index(dim3)
            iPlanta = plantas.index(dim0)
            iLinha = linhas.index(dim1)
            values = data.get(i)['Value']
            if values is  None:
                values = 0
            x[iPlanta][iLinha][iCarro][iMes] = values

    #funcao objetivo max resultado = receitas - custos
    print('fim ler dados')
    profit = []
    nomes = []
    types =[]
    po = "MAX "
    num_plantas = len(plantas)
    num_linhas = len(linhas)
    num_carros = len(carros)
    num_meses = len(meses)



    for i in range(num_plantas):
        for j in range(num_linhas):
            for v in range(num_carros):
                if pode_produzir[i][j][v] != 0:
                    for m in range(mes_atual,num_meses):
                        exec("nomes.append('x_%s_%s_%s_%s')" % (i,j,v,m))
                        profit.append(c[v][m])
                        types.append('N')
    for i in range(num_plantas):
        for v in range(num_carros):
            for m in range(mes_atual,num_meses):
                exec("nomes.append('d_%s_%s_%s')" % (i,v,m))
                profit.append(0)
                types.append('N')


    print('MAX')
    po += "\n" 
    po += "\nST\n"
    # lin_expr_variaveis = [[[],[]] for _ in range(0)]
    expr = []
    coeficientes = []
    senses = []
    lista_aux = []
    lista_aux2 = []
    lista_geral = []
    rhs = []
    names= []
    pp = 0
    #capacidade de producao 
    for i in range(num_plantas):
        for j in range(num_linhas):
            for m in range(mes_atual,num_meses):
                expr = []
                coeficientes = []
                for v in range(num_carros):
                    if pode_produzir[i][j][v] != 0:
                        exec("expr.append('x_%s_%s_%s_%s')" % (i,j,v,m))
                        coeficientes.append(1)
                senses.append('L')
                exec("names.append('c_%s')" % (pp))
                rhs.append(k[i][j][m])
                lista_aux = [expr,coeficientes]
                lista_geral += [lista_aux]
                pp+=1
    print ('restricao 1')

    #atender a demanda e maxmimo de produção
    for v in range(num_carros):
        for m in range(mes_atual,num_meses):
            expr = []
            coeficientes = []
            for i in range(num_plantas):
                exec("expr.append('d_%s_%s_%s')" % (i,v,m))
                coeficientes.append(1)
            lista_aux = [expr,coeficientes]
            lista_geral += [lista_aux]
            senses.append('G')
            exec("names.append('c_%s')" % (pp))
            rhs.append(wholesales_i[v][m])
            senses.append('L')
            exec("names.append('c_%s')" % (pp+1))
            rhs.append(wholesales_s[v][m])
            lista_aux = [expr,coeficientes]
            lista_geral += [lista_aux]
            # lista_geral += [lista_aux]
            pp+=2

    #maximo da free demand
    for v in range(num_carros):
        for m in range(mes_atual,num_meses):
            expr = []
            coeficientes = []
            for i in range(num_plantas):
                for j in range(num_linhas):
                    if pode_produzir[i][j][v] != 0:
                        exec("expr.append('x_%s_%s_%s_%s')" % (i,j,v,m))
                        coeficientes.append(1)
            senses.append('G')
            exec("names.append('c_%s')" % (pp))
            rhs.append(d[v][m])
            senses.append('L')
            exec("names.append('c_%s')" % (pp+1))
            rhs.append(ls[v][m])
            lista_aux = [expr,coeficientes]
            lista_geral += [lista_aux]
            lista_geral += [lista_aux]
            pp+=2

    print ('restricao 2')
    #restricao de eficiencia energetica
    expr = []
    coeficientes = []
    for m in range(mes_atual,num_meses):
        for v in range(num_carros):
            for i in range(num_plantas):
                exec("expr.append('d_%s_%s_%s')" % (i,v,m))
                coeficientes.append((e[v]-B))
    t = 0
    for m in range(mes_atual):
        for v in range(num_carros):
            t += (-e[v]+B)*wholesales[v][m]
    senses.append('L')
    exec("names.append('c_%s')" % (pp))
    rhs.append(t)
    pp+=1
    lista_aux = [expr,coeficientes]
    lista_geral += [lista_aux]
    print ('restricao 3')


    # controle de estoque
    for v in range(len(carros)):
        for i in range(len(plantas)):
            expr = []
            coeficientes = []
            for j in range(len(linhas)):
                if pode_produzir[i][j][v] != 0:
                    for m in range(mes_atual,num_meses):
                        exec("expr.append('x_%s_%s_%s_%s')" % (i,j,v,m))
                        coeficientes.append(1)
            for m in range(mes_atual,num_meses):
                exec("expr.append('d_%s_%s_%s')" % (i,v,m))
                coeficientes.append(-1)
            lista_aux = [expr,coeficientes]
            lista_geral += [lista_aux]
            lista_geral += [lista_aux]
            senses.append('L')
            exec("names.append('c_%s')" % (pp))
            rhs.append(estoque_final_s[i][v]-estoque_inicial[i][v])

            senses.append('G')
            exec("names.append('c_%s')" % (pp+1))
            rhs.append(estoque_final_i[i][v]-estoque_inicial[i][v])
            pp+=2

    print ('fim gerar cplex')
    # sys.exit()
    try:
        prob = cplex.Cplex()
        prob.objective.set_sense(prob.objective.sense.maximize)
        prob.variables.add(obj = profit,
                      types = types,
                      names = nomes)
        
        prob.linear_constraints.add(lin_expr = lista_geral,
                               senses = senses,
                               rhs = rhs,
                               names = names)

        # prob.set_log_stream(None)
        # prob.set_error_stream(None)
        # prob.set_warning_stream(None)
        # prob.set_results_stream(None)
        cellset = {}
        cellset_giro = {}
        prob.solve()
        row = prob.solution.get_values()
        # print(row)

        ii = 0
        
        for i in range(num_plantas):
            for j in range(num_linhas):
                for v in range(num_carros):
                    if pode_produzir[i][j][v] != 0:
                        for m in range(mes_atual,num_meses):
                            cellset[(carros[v],plantas[i],linhas[j],meses[m],'Quantidade Produzida')] = row[ii]
                            ii+=1
                    else:
                        for m in range(mes_atual,num_meses):
                            cellset[(carros[v],plantas[i],linhas[j],meses[m],'Quantidade Produzida')] = 0

        for i in range(num_plantas):
            for v in range(num_carros):
                for m in range(mes_atual,num_meses):
                    cellset_giro[(plantas[i],carros[v],meses[m])] = row[ii]
                    ii+=1
        # print(row[ii])

        tm1.cubes.cells.write_values(cubo_resultado, cellset)
        tm1.cubes.cells.write_values(cubo_wholesales_resultado, cellset_giro)
    except CplexError as exc:
        print(exc)