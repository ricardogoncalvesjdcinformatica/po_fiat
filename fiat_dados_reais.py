import configparser
import collections
import time
import json
import sys
from TM1py.Services import TM1Service
from TM1py.Objects.ElementAttribute import ElementAttribute
from TM1py.Objects.Element import Element
from TM1py.Utils.Utils import CaseAndSpaceInsensitiveDict, CaseAndSpaceInsensitiveTuplesDict
from TM1py.Objects.TM1Object import TM1Object
from TM1py.Objects.Hierarchy import Hierarchy
import numpy as np
import cplex
from cplex import Cplex
from cplex.exceptions import CplexError
from docplex.cp.model import CpoModel
import random
import math
from random import randint

with TM1Service(address='localhost', port=81245, namespace='Harmony LDAP', user='administrator', password='IBMDem0s', ssl=False) as tm1:
    cubo_demanda = 'demanda_FIAT'
    cubo_eficiencia_energetica = 'eficiencia_energetica_FIAT'
    cubo_capacidade_producao = 'capacidade'
    # cubo_consultivo = 'consultivo_FIAT'
    cubo_veiculo_margin = 'margem'
    cubo_resultado = 'resultado'
    # cubo_pode_produzir = 'pode_produzir_FIAT'
    view_name = 'dados'
    meses_nome =  ['JAN','FEB','MAR','ABR','JUN','JUL','AGO','SET','OUT','NOV','DEZ']

    #demanda
    carros =[]
    meses = []
    mes_atual = 7 #abril
    data = tm1.cubes.cells.execute_view(cubo_demanda, view_name, private=False)
    totalizador = "Total"
    for i in data:
        dim0 = i[0].split('].[')[2].replace("]","")
        dim1 = i[1].split('].[')[2].replace("]","")
        if dim0 not in carros and dim0 != totalizador:
            carros.append(dim0)
        if dim1 not in meses and dim1 != totalizador:
            meses.append(dim1)
    # cellset = {}
    # for i in carros:
    #     for j in meses:
    #         cellset[(i,j)] = randint(-3000,4000)
    # tm1.cubes.cells.write_values(cubo_veiculo_margin, cellset)
    # sys.exit()
    d = [[float('inf') for _ in meses] for _ in carros]
    original = [[float('inf') for _ in meses] for _ in carros]
    ls = [[float('inf') for _ in meses] for _ in carros]
    x = [[int(0) for _ in meses] for _ in carros]
    intervalo_superior = 0.1 # 2%, 5% e 10% 
    intervalo_inferior = 0.15 # 10% 13% 15%
    for i in data:
        if_totalizador = True
        dim0 = i[0].split('].[')[2].replace("]","")
        dim1 = i[1].split('].[')[2].replace("]","")
        if dim0 == totalizador:
            if_totalizador = False
            continue
        if dim1 == totalizador:
            if_totalizador = False
            continue
        if if_totalizador:
            iCarro = carros.index(dim0)
            iMes = meses.index(dim1)
            values = data.get(i)['Value']
            if values is  None:
                values = 0
            if values <= 0:
                values = 0
            original[iCarro][iMes] = math.ceil(values)
            x[iCarro][iMes] = math.ceil(values)
            d[iCarro][iMes] = math.ceil(values)*((1-intervalo_inferior))
            ls[iCarro][iMes] = math.ceil(values)*((1+intervalo_superior))
    
    #eficiencia
    B = 1
    e = [float('inf') for _ in carros]
    data = tm1.cubes.cells.execute_view(cubo_eficiencia_energetica, view_name, private=False)
    for i in data:
        if_totalizador = True
        dim0 = i[0].split('].[')[2].replace("]","")
        dim1 = i[1].split('].[')[2].replace("]","")
        if dim0 == totalizador:
            if_totalizador = False
            continue
        if dim1 != 'Resultado':
            if_totalizador = False
            continue
        if if_totalizador:
            iCarro = carros.index(dim0)
            values = data.get(i)['Value']
            if values is  None:
                values = 0

            e[iCarro] = values
    print('eficiencia')
    #margin de contribuição
    c = [[float('inf') for _ in meses] for _ in carros]


    data = tm1.cubes.cells.execute_view(cubo_veiculo_margin, view_name, private=False)
    for i in data:
        if_totalizador = True
        dim0 = i[0].split('].[')[2].replace("]","")
        dim1 = i[1].split('].[')[2].replace("]","")
        if dim0 == totalizador:
            if_totalizador = False
            continue
        if dim1 == totalizador:
            if_totalizador = False
            continue
        if if_totalizador:
            iCarro = carros.index(dim0)
            iMes = meses.index(dim1)
            values = data.get(i)['Value']
            if values is  None:
                values = 0
            c[iCarro][iMes] = values
    print('contribuicao')
    # capacidade de produção
    linhas = []
    plantas = []
    data = tm1.cubes.cells.execute_view(cubo_capacidade_producao, view_name, private=False)
    totalizador = "Total"
    for i in data:
        dim0 = i[1].split('].[')[2].replace("]","")
        dim1 = i[0].split('].[')[2].replace("]","")
        if dim0 not in linhas and dim0 != totalizador:
            linhas.append(dim0)
        if dim1 not in plantas and dim1 != totalizador:
            plantas.append(dim1)

    k = [[[float('inf') for _ in meses] for _ in linhas] for _ in plantas]
    for i in data:
        if_totalizador = True
        dim0 = i[1].split('].[')[2].replace("]","")
        dim1 = i[0].split('].[')[2].replace("]","")
        dim2 = i[2].split('].[')[2].replace("]","")
        if dim0 == totalizador:
            if_totalizador = False
            continue
        if dim1 == totalizador:
            if_totalizador = False
            continue
        if dim2 == totalizador:
            if_totalizador = False
            continue
        if if_totalizador:
            iLinha = linhas.index(dim0)
            iPlanta = plantas.index(dim1)
            iMes = meses.index(dim2)
            values = data.get(i)['Value']
            if values is  None:
                values = 0
            k[iPlanta][iLinha][iMes] = values

    print('capacidade')

    #funcao objetivo max resultado = receitas
    print('fim ler dados')
    profit = []
    nomes = []
    types =[]
    po = "MAX "
    num_plantas = len(plantas)
    num_linhas = len(linhas)
    num_carros = len(carros)
    num_meses = len(meses)



    for i in range(num_plantas):
        for j in range(num_linhas):
            for v in range(num_carros):
                for m in range(mes_atual,num_meses):
                    exec("nomes.append('x_%s_%s_%s_%s')" % (i,j,v,m))
                    profit.append(c[v][m])
                    types.append('N')
    print('MAX')
    po += "\n" 
    po += "\nST\n"
    expr = []
    coeficientes = []
    senses = []
    lista_aux = []
    lista_aux2 = []
    lista_geral = []
    rhs = []
    names= []
    pp = 0
    #capacidade de producao 
    for i in range(num_plantas):
        for j in range(num_linhas):
            for m in range(mes_atual,num_meses):
                expr = []
                coeficientes = []
                for v in range(num_carros):
                    exec("expr.append('x_%s_%s_%s_%s')" % (i,j,v,m))
                    coeficientes.append(1)
                senses.append('E')
                exec("names.append('c_%s')" % (pp))
                rhs.append(k[i][j][m])
                lista_aux = [expr,coeficientes]
                lista_geral += [lista_aux]
                pp+=1
    #144
    # print(lista_geral)
    # sys.exit()
    print ('restricao 1')

    # po += "\n" 
    #atender a demanda e maxmimo de produção
    for v in range(num_carros):
        for m in range(mes_atual,num_meses):
            expr = []
            coeficientes = []
            for i in range(num_plantas):
                for j in range(num_linhas):
                    exec("expr.append('x_%s_%s_%s_%s')" % (i,j,v,m))
                    coeficientes.append(1)
            senses.append('G')
            exec("names.append('c_%s')" % (pp))
            rhs.append(d[v][m])
            senses.append('L')
            exec("names.append('c_%s')" % (pp+1))
            rhs.append(ls[v][m])
            lista_aux = [expr,coeficientes]
            lista_geral += [lista_aux]
            lista_geral += [lista_aux]
            pp+=2
    print(pp)
    print ('restricao 2')
    #restricao de eficiencia energetica
    expr = []
    coeficientes = []
    for i in range(num_plantas):
        for j in range(num_linhas):
            for m in range(mes_atual,num_meses):
                for v in range(num_carros):
                    exec("expr.append('x_%s_%s_%s_%s')" % (i,j,v,m))
                    coeficientes.append(e[v]-B)
    t = 0
    for m in range(mes_atual):
        for v in range(num_carros):
            t += (-e[v]+B)*x[v][m]
    print(t)
    senses.append('L')
    exec("names.append('c_%s')" % (pp))
    rhs.append(t)
    pp+=1
    lista_aux = [expr,coeficientes]
    lista_geral += [lista_aux]
    print ('restricao 3')

    print ('fim gerar cplex')
    # sys.exit()
    try:
        prob = cplex.Cplex()
        prob.objective.set_sense(prob.objective.sense.maximize)
        prob.variables.add(obj = profit,
                      types = types,
                      names = nomes)
        
        prob.linear_constraints.add(lin_expr = lista_geral,
                               senses = senses,
                               rhs = rhs,
                               names = names)

        # prob.set_log_stream(None)
        # prob.set_error_stream(None)
        # prob.set_warning_stream(None)
        # prob.set_results_stream(None)
        cellset = {}
        prob.solve()
        row = prob.solution.get_values()

        ii = 0
        
        for i in range(num_plantas):
            for j in range(num_linhas):
                for v in range(num_carros):
                    for m in range(mes_atual,num_meses):
                        cellset[(carros[v],plantas[i],linhas[j],meses[m],'Quantidade Produzida')] = row[ii]
                        ii+=1


         
        tm1.cubes.cells.write_values(cubo_resultado, cellset)
    except CplexError as exc:
        print(exc)