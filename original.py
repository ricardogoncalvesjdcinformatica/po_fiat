import configparser
import collections
import time
import json
import sys
from TM1py.Services import TM1Service
from TM1py.Objects.ElementAttribute import ElementAttribute
from TM1py.Objects.Element import Element
from TM1py.Utils.Utils import CaseAndSpaceInsensitiveDict, CaseAndSpaceInsensitiveTuplesDict
from TM1py.Objects.TM1Object import TM1Object
from TM1py.Objects.Hierarchy import Hierarchy
import numpy as np
import cplex
from cplex import Cplex
from cplex.exceptions import CplexError
from docplex.cp.model import CpoModel
from random import randint

with TM1Service(address='localhost', port=44312, namespace='Harmony LDAP', user='administrator', password='IBMDem0s', ssl=False) as tm1:
    cubo_demanda = 'demanda_FIAT'
    cubo_eficiencia_energetica = 'eficiencia_energetica_FIAT'
    cubo_capacidade_producao = 'capacidade'
    cubo_consultivo = 'consultivo_FIAT'
    cubo_veiculo_margin = 'margem_FIAT'
    cubo_resultado = 'resultado_FIAT_ORIGINAL'
    view_name = 'dados'
    meses_nome =  ['JAN','FEB','MAR','ABR','JUN','JUL','AGO','SET','OUT','NOV','DEZ']
    # # demanda
    carros =[]
    meses = []
    mes_atual = 0 #abril
    data = tm1.cubes.cells.execute_view(cubo_demanda, view_name, private=False)
    totalizador = "Total"
    for i in data:
        dim0 = i[0].split('].[')[2].replace("]","")
        dim1 = i[1].split('].[')[2].replace("]","")
        if dim0 not in carros and dim0 != totalizador:
            carros.append(dim0)
        if dim1 not in meses and dim1 != totalizador:
            meses.append(dim1)

    d = [[float('inf') for _ in meses] for _ in carros]
    for i in data:
        if_totalizador = True
        dim0 = i[0].split('].[')[2].replace("]","")
        dim1 = i[1].split('].[')[2].replace("]","")
        dim2 = i[2].split('].[')[2].replace("]","")
        if dim0 == totalizador:
            if_totalizador = False
            continue
        if dim1 == totalizador:
            if_totalizador = False
            continue
        if if_totalizador:
            iCarro = carros.index(dim0)
            iMes = meses.index(dim1)
            values = data.get(i)['Value']
            if values is  None:
                values = 0
            if(dim2 == 'demanda minima'):
                d[iCarro][iMes] = values
    #eficiencia
    B = 12
    e = [float('inf') for _ in carros]
    data = tm1.cubes.cells.execute_view(cubo_eficiencia_energetica, view_name, private=False)
    for i in data:
        if_totalizador = True
        dim0 = i[0].split('].[')[2].replace("]","")
        if dim0 == totalizador:
            if_totalizador = False
            continue
        if if_totalizador:
            iCarro = carros.index(dim0)
            values = data.get(i)['Value']
            if values is  None:
                values = 0
            e[iCarro] = values

    #margin de contribuição
    # c = [float('inf') for _ in carros]
    c = [[float('inf') for _ in meses] for _ in carros]
    data = tm1.cubes.cells.execute_view(cubo_veiculo_margin, view_name, private=False)
    for i in data:
        if_totalizador = True
        dim0 = i[0].split('].[')[2].replace("]","")
        dim1 = i[1].split('].[')[2].replace("]","")
        # print(meses_nome[mes_atual])
        if dim1 == totalizador:
            if_totalizador = False
            continue
        if dim0 == totalizador:
            if_totalizador = False
            continue
        if if_totalizador:
            iCarro = carros.index(dim0)
            iMes = meses.index(dim1)
            values = data.get(i)['Value']
            if values is  None:
                values = 0
            c[iCarro][iMes] = values
    # print(c)
    # sys.exit()
    # capacidade de produção
    linhas = []
    plantas = []
    data = tm1.cubes.cells.execute_view(cubo_capacidade_producao, view_name, private=False)
    totalizador = "Total"
    for i in data:
        dim0 = i[0].split('].[')[2].replace("]","")
        dim1 = i[1].split('].[')[2].replace("]","")
        if dim0 not in linhas and dim0 != totalizador:
            linhas.append(dim0)
        if dim1 not in plantas and dim1 != totalizador:
            plantas.append(dim1)

    k = [[[float('inf') for _ in meses] for _ in linhas] for _ in plantas]
    for i in data:
        if_totalizador = True
        dim0 = i[0].split('].[')[2].replace("]","")
        dim1 = i[1].split('].[')[2].replace("]","")
        dim2 = i[2].split('].[')[2].replace("]","")
        if dim0 == totalizador:
            if_totalizador = False
            continue
        if dim1 == totalizador:
            if_totalizador = False
            continue
        if dim2 == totalizador:
            if_totalizador = False
            continue
        if if_totalizador:
            iLinha = linhas.index(dim0)
            iPlanta = plantas.index(dim1)
            iMes = meses.index(dim2)
            values = data.get(i)['Value']
            if values is  None:
                values = 0
            k[iPlanta][iLinha][iMes] = values

    #dados anteriores
    x = [[[[int(randint(1, 10)) for _ in meses] for _ in carros] for _ in linhas] for _ in plantas]
    data = tm1.cubes.cells.execute_view(cubo_consultivo, view_name, private=False)
    for i in data:
        if_totalizador = True
        dim0 = i[0].split('].[')[2].replace("]","")
        dim1 = i[1].split('].[')[2].replace("]","")
        dim2 = i[2].split('].[')[2].replace("]","")
        dim3 = i[3].split('].[')[2].replace("]","")
        if dim0 == totalizador:
            if_totalizador = False
            continue
        if dim1 == totalizador:
            if_totalizador = False
            continue
        if dim2 == totalizador:
            if_totalizador = False
            continue
        if dim3 == totalizador:
            if_totalizador = False
            continue
        if if_totalizador:
            iMes = meses.index(dim2)
            iCarro = carros.index(dim3)
            iPlanta = plantas.index(dim0)
            iLinha = linhas.index(dim1)
            values = data.get(i)['Value']
            if values is  None:
                values = 0
            x[iPlanta][iLinha][iCarro][iMes] = values

    #funcao objetivo max resultado = receitas - custos
    profit = []
    nomes = []
    types =[]
    po = "MAX "

    for i in range(len(plantas)):
        for j in range(len(linhas)):
            for v in range(len(carros)):
                for m in range(mes_atual,len(meses)):
                    exec("nomes.append('x_%s_%s_%s_%s')" % (i,j,v,m))
                    profit.append(c[v][m])
                    types.append('I')
                    po+= (" +" if c[v][m]>=0 else " ")+str(c[v][m])+"x_"+str(i)+"_"+str(j)+"_"+str(v)+"_"+str(m)

    po += "\n" 
    po += "\nST\n"
    lin_expr_variaveis = [[[],[]] for _ in range((len(plantas)*len(linhas)*len(meses))+(len(carros)*len(meses))+1+(len(carros)*len(plantas)*len(linhas)*len(meses)))]
    senses = []
    rhs = []
    names= []
    pp = 0
    #capacidade de producao 
    for i in range(len(plantas)):
        for j in range(len(linhas)):
            for m in range(mes_atual,len(meses)):
                for v in range(len(carros)):
                    po+= " +x_"+str(i)+"_"+str(j)+"_"+str(v)+"_"+str(m)
                    exec("lin_expr_variaveis[%d][0].append('x_%s_%s_%s_%s')" % (pp,i,j,v,m))
                    lin_expr_variaveis[pp][1].append(1)
                senses.append('E')
                exec("names.append('c_%s')" % (pp))
                rhs.append(k[i][j][m])
                pp+=1
                po+= " <= "+str(k[i][j][m])+"\n"

    po += "\n" 
    #atender a demanda
    for m in range(mes_atual,len(meses)):
        for v in range(len(carros)):
            for i in range(len(plantas)):
                for j in range(len(linhas)):
                    po+= " +x_"+str(i)+"_"+str(j)+"_"+str(v)+"_"+str(m)
                    exec("lin_expr_variaveis[%d][0].append('x_%s_%s_%s_%s')" % (pp,i,j,v,m))
                    lin_expr_variaveis[pp][1].append(1)
            senses.append('G')
            exec("names.append('c_%s')" % (pp))
            rhs.append(d[v][m])
            pp+=1
            po+= " >= "+str(d[v][m])+"\n"

    po += "\n" 
    #restricao de eficiencia energetica
    for i in range(len(plantas)):
        for j in range(len(linhas)):
            for v in range(len(carros)):
                for m in range(mes_atual,len(meses)):
                    exec("lin_expr_variaveis[%d][0].append('x_%s_%s_%s_%s')" % (pp,i,j,v,m))
                    lin_expr_variaveis[pp][1].append(e[v]-B)
                    po+= (" +" if e[v]-B>=0 else " ") +str(e[v]-B)+"x_"+str(i)+"_"+str(j)+"_"+str(v)+"_"+str(m)
    t = 0
    for i in range(len(plantas)):
        for j in range(len(linhas)):
            for v in range(len(carros)):
                for m in range(mes_atual):
                    t += (e[v]-B)*x[i][j][v][m]
    senses.append('L')
    exec("names.append('c_%s')" % (pp))
    rhs.append(t)
    pp+=1
    po+= " <= "+str(t)+"\n"
    po += "\n" 

    #restricao de positivo 
    for i in range(len(plantas)):
        for j in range(len(linhas)):
            for v in range(len(carros)):
                for m in range(mes_atual,len(meses)):
                    exec("lin_expr_variaveis[%d][0].append('x_%s_%s_%s_%s')" % (pp,i,j,v,m))
                    lin_expr_variaveis[pp][1].append(1)
                    senses.append('G')
                    exec("names.append('c_%s')" % (pp))
                    rhs.append(0)
                    pp+=1
                    po+= " +x_"+str(i)+"_"+str(j)+"_"+str(v)+"_"+str(m)+" >= 0\n"
    po += "\n" 

    try:
        prob = cplex.Cplex()
        prob.objective.set_sense(prob.objective.sense.maximize)
        prob.variables.add(obj = profit,
                      types = types,
                      names = nomes)
        
        prob.linear_constraints.add(lin_expr = lin_expr_variaveis,
                               senses = senses,
                               rhs = rhs,
                               names = names)

        # prob.set_log_stream(None)
        # prob.set_error_stream(None)
        # prob.set_warning_stream(None)
        # prob.set_results_stream(None)
        cellset = {}
        prob.solve()
        row = prob.solution.get_values()
        ii = 0
        for i in range(len(plantas)):
            for j in range(len(linhas)):
                for v in range(len(carros)):
                    for m in range(mes_atual,len(meses)):
                        cellset[(carros[v],plantas[i],linhas[j],meses[m],'Quantidade Produzida')] = row[ii]
                        ii+=1

        tm1.cubes.cells.write_values(cubo_resultado, cellset)
    except CplexError as exc:
        print(exc)