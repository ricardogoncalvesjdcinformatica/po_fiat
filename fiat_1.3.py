import configparser
import collections
import time
import json
import sys
from TM1py.Services import TM1Service
from TM1py.Objects.ElementAttribute import ElementAttribute
from TM1py.Objects.Element import Element
from TM1py.Utils.Utils import CaseAndSpaceInsensitiveDict, CaseAndSpaceInsensitiveTuplesDict
from TM1py.Objects.TM1Object import TM1Object
from TM1py.Objects.Hierarchy import Hierarchy
import numpy as np
import cplex
from cplex import Cplex
from cplex.exceptions import CplexError
from docplex.cp.model import CpoModel
import random
from random import randint
import math

with TM1Service(address='localhost', port=81234, namespace='Harmony LDAP', user='administrator', password='IBMDem0s', ssl=False) as tm1:
    cubo_demanda = 'demanda_FIAT'
    cubo_eficiencia_energetica = 'eficiencia_energetica_FIAT'
    cubo_capacidade_producao = 'capacidade'
    cubo_consultivo = 'consultivo_FIAT'
    cubo_veiculo_margin = 'margem_FIAT'
    cubo_resultado = 'resultado_FIAT_ORIGINAL'
    cubo_pode_produzir = 'pode_produzir_FIAT'
    cubo_estoque = 'estoque_FIAT'
    view_name = 'dados'
    meses_nome =  ['JAN','FEB','MAR','ABR','JUN','JUL','AGO','SET','OUT','NOV','DEZ']

    #demanda
    carros =[]
    meses = []
    mes_atual = 0 #abril
    data = tm1.cubes.cells.execute_view(cubo_demanda, view_name, private=False)
    totalizador = "Total"
    for i in data:
        dim0 = i[0].split('].[')[2].replace("]","")
        dim1 = i[1].split('].[')[2].replace("]","")
        if dim0 not in carros and dim0 != totalizador:
            carros.append(dim0)
        if dim1 not in meses and dim1 != totalizador:
            meses.append(dim1)
    d = [[float('inf') for _ in meses] for _ in carros]
    original = [[float('inf') for _ in meses] for _ in carros]
    ls = [[float('inf') for _ in meses] for _ in carros]
    emplacados = [[float('inf') for _ in meses] for _ in carros]
    # cellset = {}
    # for i in carros:
    #     for j in meses:
    #         cellset[(i,j,'Limite Inferior')] = 1
    #         cellset[(i,j,'Estoque Final')] = randint(500,700)
    # tm1.cubes.cells.write_values(cubo_estoque, cellset)
    # sys.exit()
    # cellset = {}
    # for i in carros:
    #     for j in meses:
    #         cellset[(i,j,'Limite Inferior')] = 0
    #         # cellset[(i,j,'Limite Superior')] = random.uniform(0,1)
    # tm1.cubes.cells.write_values(cubo_estoque, cellset)
    # sys.exit()

    for i in data:
        if_totalizador = True
        dim0 = i[0].split('].[')[2].replace("]","")
        dim1 = i[1].split('].[')[2].replace("]","")
        dim2 = i[2].split('].[')[2].replace("]","")
        if dim0 == totalizador:
            if_totalizador = False
            continue
        if dim1 == totalizador:
            if_totalizador = False
            continue
        if if_totalizador:
            iCarro = carros.index(dim0)
            iMes = meses.index(dim1)
            values = data.get(i)['Value']
            if(dim2 == 'demanda minima'):
                if values is  None:
                    values = 0
                d[iCarro][iMes] = values
                original[iCarro][iMes] = values
            if(dim2 == 'limite inferior'):
                if values is  None:
                    values = 0
                d[iCarro][iMes] = d[iCarro][iMes]*((1-values))
            if(dim2 == 'limite superior'):
                if values is  None:
                    values = 0
                ls[iCarro][iMes] = original[iCarro][iMes]*((1+values))
            if(dim2 == 'emplacados'):
                if values is  None:
                    values = 0
                emplacados[iCarro][iMes] = values
    # print()
    # estoque
    estoque_inicial = [[float('inf') for _ in meses] for _ in carros]
    estoque_final = [[float('inf') for _ in meses] for _ in carros]
    estoque_final_s = [[float('inf') for _ in meses] for _ in carros]
    estoque_final_i = [[float('inf') for _ in meses] for _ in carros]
    data = tm1.cubes.cells.execute_view(cubo_estoque, view_name, private=False)
    for i in data:
        if_totalizador = True
        dim0 = i[0].split('].[')[2].replace("]","")
        dim1 = i[1].split('].[')[2].replace("]","")
        dim2 = i[2].split('].[')[2].replace("]","")
        if dim0 == totalizador:
            if_totalizador = False
            continue
        if dim1 == totalizador:
            if_totalizador = False
            continue
        if if_totalizador:
            iCarro = carros.index(dim0)
            iMes = meses.index(dim1)
            values = data.get(i)['Value']
            if(dim2 == 'Estoque Inicial'):
                if values is  None:
                    values = 0
                estoque_inicial[iCarro][iMes] = values
            if(dim2 == 'Estoque Final'):
                if values is  None:
                    values = 0
                estoque_final[iCarro][iMes] = values
            if(dim2 == 'Limite Inferior'):
                if values is  None:
                    values = 0
                estoque_final_i[iCarro][iMes] = estoque_final[iCarro][iMes]*((1-values))
            if(dim2 == 'Limite Superior'):
                if values is  None:
                    values = 0
                estoque_final_s[iCarro][iMes] = estoque_final[iCarro][iMes]*((1+values))
    # print(estoque_final_i)
    # sys.exit()        

    #eficiencia
    B = 12
    e = [float('inf') for _ in carros]
    data = tm1.cubes.cells.execute_view(cubo_eficiencia_energetica, view_name, private=False)
    for i in data:
        if_totalizador = True
        dim0 = i[0].split('].[')[2].replace("]","")
        if dim0 == totalizador:
            if_totalizador = False
            continue
        if if_totalizador:
            iCarro = carros.index(dim0)
            values = data.get(i)['Value']
            if values is  None:
                values = 0
            e[iCarro] = values

    print('eficiencia')
    #margin de contribuição
    c = [[float('inf') for _ in meses] for _ in carros]

    data = tm1.cubes.cells.execute_view(cubo_veiculo_margin, view_name, private=False)
    for i in data:
        if_totalizador = True
        dim0 = i[0].split('].[')[2].replace("]","")
        dim1 = i[1].split('].[')[2].replace("]","")
        # print(dim0,dim1)
        # print(meses_nome[mes_atual])
        # if dim1 != meses[mes_atual]:
        #     if_totalizador = False
        #     continue
        if dim0 == totalizador:
            if_totalizador = False
            continue
        if dim1 == totalizador:
            if_totalizador = False
            continue
        if if_totalizador:
            iCarro = carros.index(dim0)
            iMes = meses.index(dim1)
            values = data.get(i)['Value']
            if values is  None:
                values = 0
            c[iCarro][iMes] = values
    print('contribuicao')

    # capacidade de produção
    linhas = []
    plantas = []
    data = tm1.cubes.cells.execute_view(cubo_capacidade_producao, view_name, private=False)
    totalizador = "Total"
    for i in data:
        dim0 = i[0].split('].[')[2].replace("]","")
        dim1 = i[1].split('].[')[2].replace("]","")
        if dim0 not in linhas and dim0 != totalizador:
            linhas.append(dim0)
        if dim1 not in plantas and dim1 != totalizador:
            plantas.append(dim1)

    k = [[[float('inf') for _ in meses] for _ in linhas] for _ in plantas]
    for i in data:
        if_totalizador = True
        dim0 = i[0].split('].[')[2].replace("]","")
        dim1 = i[1].split('].[')[2].replace("]","")
        dim2 = i[2].split('].[')[2].replace("]","")
        if dim0 == totalizador:
            if_totalizador = False
            continue
        if dim1 == totalizador:
            if_totalizador = False
            continue
        if dim2 == totalizador:
            if_totalizador = False
            continue
        if if_totalizador:
            iLinha = linhas.index(dim0)
            iPlanta = plantas.index(dim1)
            iMes = meses.index(dim2)
            values = data.get(i)['Value']
            if values is  None:
                values = 0
            k[iPlanta][iLinha][iMes] = values

    print('capacidade')

    #verifica se linha planta pode produzir mvs 1 = true 0 = false
    pode_produzir = [[[int(0) for _ in carros] for _ in linhas] for _ in plantas]
    data = tm1.cubes.cells.execute_view(cubo_pode_produzir, view_name, private=False)
    num_restricao_produzir = 0
    for i in data:
        if_totalizador = True
        dim0 = i[0].split('].[')[2].replace("]","")
        dim1 = i[1].split('].[')[2].replace("]","")
        dim2 = i[2].split('].[')[2].replace("]","")
        if dim0 == totalizador:
            if_totalizador = False
            continue
        if dim1 == totalizador:
            if_totalizador = False
            continue
        if dim2 == totalizador:
            if_totalizador = False
            continue
        if if_totalizador:
            iCarro = carros.index(dim2)
            iPlanta = plantas.index(dim0)
            iLinha = linhas.index(dim1)
            values = data.get(i)['Value']
            if values is  None:
                values = 0
                num_restricao_produzir+=1
            pode_produzir[iPlanta][iLinha][iCarro] = values

    #consultivo
    x = [[[[int(0) for _ in meses] for _ in carros] for _ in linhas] for _ in plantas]
    data = tm1.cubes.cells.execute_view(cubo_consultivo, view_name, private=False)
    for i in data:
        if_totalizador = True
        dim0 = i[0].split('].[')[2].replace("]","")
        dim1 = i[1].split('].[')[2].replace("]","")
        dim2 = i[2].split('].[')[2].replace("]","")
        dim3 = i[3].split('].[')[2].replace("]","")
        if dim0 == totalizador:
            if_totalizador = False
            continue
        if dim1 == totalizador:
            if_totalizador = False
            continue
        if dim2 == totalizador:
            if_totalizador = False
            continue
        if dim3 == totalizador:
            if_totalizador = False
            continue
        if if_totalizador:
            iMes = meses.index(dim2)
            iCarro = carros.index(dim3)
            iPlanta = plantas.index(dim0)
            iLinha = linhas.index(dim1)
            values = data.get(i)['Value']
            if values is  None:
                values = 0
            x[iPlanta][iLinha][iCarro][iMes] = values

    #funcao objetivo max resultado = receitas - custos
    print('fim ler dados')
    profit = []
    nomes = []
    types =[]
    po = "MAX "
    num_plantas = len(plantas)
    num_linhas = len(linhas)
    num_carros = len(carros)
    num_meses = len(meses)



    for i in range(num_plantas):
        for j in range(num_linhas):
            for v in range(num_carros):
                if pode_produzir[i][j][v] != 0:
                    for m in range(mes_atual,num_meses):
                        exec("nomes.append('x_%s_%s_%s_%s')" % (i,j,v,m))
                        profit.append(c[v][m])
                        types.append('I')
                    # print(i,j,v,m)
    print('MAX')
    po += "\n" 
    po += "\nST\n"
    # lin_expr_variaveis = [[[],[]] for _ in range(0)]
    expr = []
    coeficientes = []
    senses = []
    lista_aux = []
    lista_aux2 = []
    lista_geral = []
    rhs = []
    names= []
    pp = 0
    #capacidade de producao 
    for i in range(num_plantas):
        for j in range(num_linhas):
            for m in range(mes_atual,num_meses):
                expr = []
                coeficientes = []
                for v in range(num_carros):
                    if pode_produzir[i][j][v] != 0:
                        exec("expr.append('x_%s_%s_%s_%s')" % (i,j,v,m))
                        coeficientes.append(1)
                        # exec("lin_expr_variaveis[%d][0].append('x_%s_%s_%s_%s')" % (pp,i,j,v,m))
                        # lin_expr_variaveis[pp][1].append(1)
                senses.append('L')
                exec("names.append('c_%s')" % (pp))
                rhs.append(k[i][j][m])
                lista_aux = [expr,coeficientes]
                lista_geral += [lista_aux]
                pp+=1
    #144
    # print(pp)
    # sys.exit()
    # print(lista_geral)
    # sys.exit()
    print ('restricao 1')

    # po += "\n" 
    #atender a demanda e maxmimo de produção
    for v in range(num_carros):
        for m in range(mes_atual,num_meses):
            expr = []
            coeficientes = []
            for i in range(num_plantas):
                for j in range(num_linhas):
                    if pode_produzir[i][j][v] != 0:
                        # exec("lin_expr_variaveis[%d][0].append('x_%s_%s_%s_%s')" % (pp,i,j,v,m))
                        # lin_expr_variaveis[pp][1].append(1)
                        # exec("lin_expr_variaveis[%d][0].append('x_%s_%s_%s_%s')" % (pp+1,i,j,v,m))
                        # lin_expr_variaveis[pp+1][1].append(1)
                        exec("expr.append('x_%s_%s_%s_%s')" % (i,j,v,m))
                        coeficientes.append(1)
            senses.append('G')
            exec("names.append('c_%s')" % (pp))
            rhs.append(d[v][m])
            senses.append('L')
            exec("names.append('c_%s')" % (pp+1))
            rhs.append(ls[v][m])
            lista_aux = [expr,coeficientes]
            lista_geral += [lista_aux]
            lista_geral += [lista_aux]
            pp+=2

    print ('restricao 2')
    #restricao de eficiencia energetica
    expr = []
    coeficientes = []
    for i in range(num_plantas):
        for j in range(num_linhas):
            for m in range(mes_atual,num_meses):
                for v in range(num_carros):
                    if pode_produzir[i][j][v] != 0:
                        # exec("lin_expr_variaveis[%d][0].append('x_%s_%s_%s_%s')" % (pp,i,j,v,m))
                        # lin_expr_variaveis[pp][1].append(e[v]-B)
                        exec("expr.append('x_%s_%s_%s_%s')" % (i,j,v,m))
                        coeficientes.append((e[v]-B)*emplacados[v][m])
    t = 0
    for m in range(mes_atual):
        for i in range(num_plantas):
            for j in range(num_linhas):
                for v in range(num_carros):
                    t += (e[v]-B)*x[i][j][v][m]*emplacados[v][m]
    senses.append('L')
    exec("names.append('c_%s')" % (pp))
    rhs.append(t)
    pp+=1
    lista_aux = [expr,coeficientes]
    lista_geral += [lista_aux]
    print ('restricao 3')



    #restricao de positivo 
    for i in range(num_plantas):
        for j in range(num_linhas):
            for v in range(num_carros):
                if pode_produzir[i][j][v] != 0:
                    for m in range(mes_atual,num_meses):
                        expr = []
                        coeficientes = []
                        exec("expr.append('x_%s_%s_%s_%s')" % (i,j,v,m))
                        coeficientes.append(1)
                        lista_aux = [expr,coeficientes]
                        lista_geral += [lista_aux]
                        # exec("lin_expr_variaveis[%d][0].append('x_%s_%s_%s_%s')" % (pp,i,j,v,m))
                        # lin_expr_variaveis[pp][1].append(1)
                        senses.append('G')
                        exec("names.append('c_%s')" % (pp))
                        rhs.append(0)
                        pp+=1
    # print(pp)
    # sys.exit()
    print ('restricao 4')
    # controle de estoque
    for v in range(len(carros)):
        expr = []
        coeficientes = []
        for m in range(mes_atual,len(meses)):
            for i in range(len(plantas)):
                for j in range(len(linhas)):
                    if pode_produzir[i][j][v] != 0:
                        exec("expr.append('x_%s_%s_%s_%s')" % (i,j,v,m))
                        coeficientes.append(1-emplacados[v][m])
                        # exec("lin_expr_variaveis[%d][0].append('x_%s_%s_%s_%s')" % (pp,i,j,v,m))
                        # lin_expr_variaveis[pp][1].append(1)
                        # po+= " +x_"+str(i)+"_"+str(j)+"_"+str(v)+"_"+str(m)

        t = math.ceil(estoque_final_s[v][mes_atual] - estoque_inicial[v][mes_atual])
        t2 = int(estoque_final_i[v][mes_atual] - estoque_inicial[v][mes_atual])
        # print(t2)
        # print(coeficientes)
        # po+= " = "+str(t)+"\n"
        lista_aux = [expr,coeficientes]
        lista_geral += [lista_aux]
        senses.append('L')
        exec("names.append('c_%s')" % (pp))
        rhs.append(t)
        pp+=1
        lista_aux = [expr,coeficientes]
        lista_geral += [lista_aux]
        senses.append('G')
        exec("names.append('c_%s')" % (pp))
        rhs.append(t2)
        pp+=1
    # print(lista_geral)
    # sys.exit()
    print ('fim gerar cplex')
    # sys.exit()
    try:
        prob = cplex.Cplex()
        prob.objective.set_sense(prob.objective.sense.maximize)
        prob.variables.add(obj = profit,
                      types = types,
                      names = nomes)
        
        prob.linear_constraints.add(lin_expr = lista_geral,
                               senses = senses,
                               rhs = rhs,
                               names = names)

        # prob.set_log_stream(None)
        # prob.set_error_stream(None)
        # prob.set_warning_stream(None)
        # prob.set_results_stream(None)
        cellset = {}
        prob.solve()
        row = prob.solution.get_values()

        ii = 0
        
        for i in range(num_plantas):
            for j in range(num_linhas):
                for v in range(num_carros):
                    if pode_produzir[i][j][v] != 0:
                        for m in range(mes_atual,num_meses):
                            cellset[(carros[v],plantas[i],linhas[j],meses[m],'Quantidade Produzida')] = row[ii]
                            cellset[(carros[v],plantas[i],linhas[j],meses[m],'Estoque')] = math.ceil(row[ii] * (1-emplacados[v][m]))
                            ii+=1
                    else:
                        for m in range(mes_atual,num_meses):
                            cellset[(carros[v],plantas[i],linhas[j],meses[m],'Quantidade Produzida')] = 0
                            cellset[(carros[v],plantas[i],linhas[j],meses[m],'Estoque')] = 0

        tm1.cubes.cells.write_values(cubo_resultado, cellset)
    except CplexError as exc:
        print(exc)